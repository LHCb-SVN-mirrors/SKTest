def test_import():
    # this should not fail
    import TheTestPackage

def test_got_submodule():
    import TheTestPackage
    assert hasattr(TheTestPackage, 'a_submodule')
